resource "google_project_iam_custom_role" "my-custom-role" {
  role_id     = var.roleid
  title       = var.title
  permissions = var.permissions
}