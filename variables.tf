variable "project" {
  type = string
}
variable "region" {
  type = string
}
variable "roleid" {
  type = string
}
variable "title" {
  type = string
}
variable "permissions" {
  type = list(string)
}